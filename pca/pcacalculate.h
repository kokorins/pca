#ifndef PCACALCULATE_H_
#define PCACALCULATE_H_
#include <vector>
#include <boost/shared_ptr.hpp>

namespace pca {
  struct PcaParameters;
  class DataModel;
class PcaCalculate {
public:
  PcaCalculate();
  long numComps() const;
  const std::vector<double>& lambda()const;
  bool getLoadings(size_t idx, std::vector<double>& pP1)const;
  bool getScores(size_t idx, std::vector<double>& pX1)const;
  double sigLevel() const;
  void setSigLevel(double val);
  bool getStdMeans(std::vector<double>& Means, std::vector<double>& SS)const;
  bool getStdCases(std::vector<std::vector<double> >& StCases) const;
  double limEigValue()const;
  void setLimEigValue(double val);
  bool isStandardize()const;
  void setStandardize(bool val);
  long numCases() const;
  long numVars() const;
  const std::vector<std::vector<double> >& factors()const;
  const std::vector<std::vector<double> >& scores()const;
public:
	bool calcHottellingEllipse(int idx_x, int idx_y, double &XAxes, double &YAxes);
	bool standardize();
	bool setCase(long Number, const std::vector<double>& Variables);
  bool setCase(const std::vector<std::vector<double> >& Variables);
  bool setData(const std::vector<std::vector<double> >& data);
	bool calculate(PcaParameters * const params=0);
  double calcExplained(size_t num_comp)const;
private:
  bool setData(long NCase, long NVariables);
  bool setData(long NCase, long NVariables, const std::vector<std::vector<double> >& data);
public:
  boost::shared_ptr<DataModel>	_data;
};
} //namespace pca
#endif //__PCACALCULATE_H_
