#include "calculationfunctions.h"
#include <cmath>
#include <algorithm>
#include <numeric>
#include <functional>

namespace pca {

struct iota {
  int idx_;
public:
  iota(int idx):idx_(idx) {}
  iota():idx_(0) {}
  int operator()() {
    return idx_++;
  }
};

template <typename T>
class IndirectOrder : std::binary_function<bool, size_t, size_t> {
public:
  explicit IndirectOrder(const std::vector<T>& indirect, bool is_desc = false) :
    _indirect(indirect), _is_desc(is_desc) {}
  bool operator()(size_t lhs, size_t rhs)const {
    if(_is_desc)
      return _indirect[lhs]>_indirect[rhs];
    else
      return _indirect[lhs]<_indirect[rhs];
  }
private:
  const std::vector<T>& _indirect;
  bool _is_desc;
};

double Signum(double x)
{
	if(x>0.0)
    return 1.0;
	if(x<0.0)
    return -1.0;
	return 0.0;
}

int Jakobi(const std::vector<std::vector<double> >& Abeg, 
           std::vector<double>& Lambda, 
           std::vector<std::vector<double> >& Xlambda)
{
  size_t N = Abeg.size();
  std::vector<std::vector<double> > A(Abeg);
  for(size_t i=0; i<N; ++i) {
    std::fill(Xlambda[i].begin(), Xlambda[i].end(), 0);
    Xlambda[i][i] = 1.0;
  }
  if( N == 1 ) {
    Lambda[0] = 1.0;		
    return _PCAErrNoErrors;
  }

double t,s,c,zi,zj;
int i,j,k,i0,j0;
	
  while(FindMaxTri(A,i0,j0)>0.00000001) {
	  //'transform A-only upper half
	  //'1.Rotation angle
	  double dzeta = (A[j0][j0] - A[i0][i0]) / (2.0 * A[i0][j0]);

	  if(std::fabs(dzeta) < 0.000000001)
      t = 1.0;
	  else 
      t = Signum(dzeta) / (fabs(dzeta) + sqrt(1.0 + dzeta * dzeta));

	  c = 1 / sqrt(1.0 + t * t);
	  s = c * t;
		//'2.Rotate
	  for(k = 0;k<i0;k++) {
		  zi = A[k][i0];
		  zj = A[k][j0];
		  A[k][i0] = c * zi - s * zj;
		  A[k][j0] = s * zi + c * zj;
	  }//Next k
	  for(k = i0+1;k<j0;k++) {
		  zi = A[i0][k];
		  zj = A[k][j0];
		  A[i0][k] = c * zi - s * zj;
		  A[k][j0] = s * zi + c * zj;
	  }//Next k
	  for(k = j0+1;k<N;k++) {
		  zi = A[i0][k];
		  zj = A[j0][k];
		  A[i0][k] = c * zi - s * zj;
		  A[j0][k] = s * zi + c * zj;
	  }//Next k
	
	  A[i0][i0] = A[i0][i0] - A[i0][j0] * t;
	  A[j0][j0] = A[j0][j0] + A[i0][j0] * t;
	  A[i0][j0] = 0;
	  //'transform Xlambda
	  for(k = 0; k<N; k++) {
		  zi = Xlambda[i0][k];
		  zj = Xlambda[j0][k];
		  Xlambda[i0][k] = c * zi - s * zj;
		  Xlambda[j0][k] = s * zi + c * zj;
	  }//Next k
  }//while
  for(i=0; i<N; i++)
    Lambda[i] = A[i][i];
	for(i=0; i<N; i++) {
		double m=-1.0;
		for(j=0;j<N;j++) {
			if(fabs(Xlambda[i][j]) > m) {
				j0 = j;
				m = fabs(Xlambda[i][j]);
			}
    }
		if (Xlambda[i][j0]<0)
			for(j=0;j<N;j++) 
        Xlambda[i][j]=-Xlambda[i][j];
	}
	return _PCAErrNoErrors;
}

void Reorder(std::vector<double>& lambda,
             std::vector<std::vector<double> >& Xlambda)
{
  size_t sz = lambda.size();
  std::vector<size_t> old_idxs(sz);
  std::generate(old_idxs.begin(), old_idxs.end(), iota());
  std::sort(old_idxs.begin(), old_idxs.end(), IndirectOrder<double>(lambda, true));

  std::vector<std::vector<double> > loc_x(sz, std::vector<double>(sz));
  std::vector<double> loc_lam(sz);
  for(size_t i=0; i<sz; ++i) {
    loc_x[i] = Xlambda[old_idxs[i]];
    loc_lam[i] = lambda[old_idxs[i]];
  }
  std::swap(loc_x, Xlambda);
  std::swap(loc_lam, lambda);
}

double FisherInverse(double p, int n1, int n2)
{
//' 2     n1,n2>2
  double B1 = 0;
//    '    F
  double B2 = 4;
  while (Fdist(B2, n1, n2) > p)
    B2 *= 2.0;

  for(int iter=0; iter<=200; ++iter) {
    double a = (B1 + B2) / 2.0;
    double dist_val = Fdist(a, n1, n2);
    if((fabs(dist_val - p) < 1e-10))
			return a;
    if (dist_val > p)
      B1 = a;
    else
      B2 = a;
  }
  return (B1+B2)/2;
}

double Fdist(double F,int n1,int n2)
{//'Fdist= ,  ,    F(n1,n2),
//'         F
	double rez;
	int i,ii;
	double x,sl,st;
	double t,a,b,cost,sint;

    x = n2 / (n2 + n1 * F);
    //' n1 -
  if((n1%2)== 0) {
    rez = 1;
    sl = 1;
    ii=(n1 - 2) / 2;
    for (i = 1;i<=ii;i++)	{
			sl = (sl * (1 - x) / (2 * i)) * (n2 + 2 * (i - 1));
			rez = rez + sl;
		}
    if((n2 % 2) == 0) {
			ii=n2/2;
      for(i = 1;i<=ii;i++)
				rez = rez * x;
    }
    else {
			rez = rez * sqrt(x);
      ii=(n2 - 1) / 2;
      for(i =1; i<=ii; i++)
        rez=rez * x;
		}
	}
  else {
		//' n2 -
    if((n2%2) == 0) {
			rez = 1;
			sl = 1;
			ii=(n2 - 2) / 2;
			for( i = 1;i<=ii;i++)
			{	
				sl = (sl * (n1 + 2 * (i - 1)) * x) / (2 * i);
				rez = rez + sl;
			}
			rez=rez * sqrt(1.0 - x);
      ii=(n1 - 1) / 2;
			for( i = 1;i<=ii;i++)
				rez=rez* (1.0 - x);
      rez=1.0-rez;
		}
    else {	//' n1  n2 -
			t = atan(sqrt(n1 * F / n2));
			cost = cos(t);
			sint = sin(t);
            
			a = 2 * t / pi;
      if(n2 > 1) {
				sl = 2 * sint * cost / pi;
				a = a + sl;
				ii=(n2 - 3) / 2;
        for(i = 1; i<=ii; i++) {
					sl = sl * 2 * i * cost * cost / (2 * i + 1);
					a = a + sl;
				}
			}
			b = 0;
      if(n1 > 1) {
				st = 1;
				b = 1;
				ii=(n1 - 3) /2;
				for (i = 1;i<=ii;i++) 
				{
					st = st * (n2 + 2 * (i - 1) + 1) * sint * sint / (2 * i + 1);
					b = b + st;
				}
				b = b * 2 * sint / sqrt(pi);
				for( i = 1; i<= n2;i++)
				{
					b = b * cost;
				}
				b = b * Gamm(n2);
			}     
			rez = 1 - a + b;
		}
	}	

  if (rez < 1e-12)
    rez=0;
  return rez;
//Exit Function

//ErrHandler1:
//    Select Case Err.Number
//        Case 6 '     ,
//               ' P-   
  x = ((exp(log(F)*0.3333333333333)) * (1 - 1 / n2) - (1 - 2 / (9 * n1))) / sqrt(2 / (9 * n1) + 2 * ((exp(log(F)*0.666666666666667)) / (9 * n2)));
  rez=Ndist(x);
  return rez;
}

double Tdist(double F,int n2)
{
	//'Tdist= ,  ,    T(n),
	//' ,    F
	int i,ii;
	double t,a,cost,sint,sl;
  double rez;

	t = atan(F / sqrt((double)n2));
	cost = cos(t);
	sint = sin(t);
	//' n -
  if((n2%2) == 1) {
		a = 2 * t / pi;
    if( n2 > 1) {
			sl = 2 * sint * cost / pi;
            a = a + sl;
			ii=(n2 - 3) / 2;
      for(i = 1;i<=ii;i++) {
				sl = sl * 2 * i * cost * cost / (2 * i + 1);
				a = a + sl;
			}
		}
  }
  else {
		sl = sint;
		a = sl;
		ii=(n2 - 2) / 2;
    for(i = 1;i<=ii;i++) {
			sl = sl * (2 * i - 1) * cost * cost / (2 * i);
			a = a + sl;
		}
	}
    
	rez = 1 - a;
  if(rez < 0.000000000001)
    rez = 0;
  return rez;
}

double Ndist(double n)
{
//'Ndist= ,  ,    N(0,1),
//'         N ( - 4- )
	double rez,x;

  if(n > 10)
    return 0.0;
	x=exp(log(n*(n*(n*(n*0.019527+0.000344)+0.115194)+0.196854)+1)*4.0);
	rez=1.0/(2*(x));
  return rez;
}

double Gamm(int n)
{
//'((n-1)/2)!/(((n-2)/2)!). n - 
	int i,ii;
	double rez;
  if(n == 1) {
		rez=1.0/ sqrt(pi);
		return rez;
	}
  if(n == 3) {
		rez= 2 / sqrt(pi);
		return rez;
	}
	rez = 1.0 / sqrt(pi);
	ii=(n - 1) / 2;
  for(i=1; i<=ii; i++)
		rez = rez * i * 2 / (2 * i - 1);
  return rez;
}

bool LinRegr(const std::vector<double> X,
             const std::vector<double>& Y,
             double &a,double &b)
{
	//postroenie linejnoj regr Y=a*X+b
  size_t N = X.size();
  if(N<2)
    return false;
  double Sx = std::accumulate(X.begin(), X.end(), 0.0);
  double Sy = std::accumulate(Y.begin(), Y.end(), 0.0);
  double Sxy = std::inner_product(X.begin(), X.end(), Y.begin(), 0.0);
  double Sx2 = std::inner_product(X.begin(), X.end(), X.begin(), 0.0);
//  double Sy2 = std::inner_product(Y.begin(), Y.end(), Y.begin(), 0.0);
  double Qxy = Sxy - Sx * (Sy / N);
  double Qx = Sx2 - Sx * (Sx / N);
  double XMean = Sx / N;
  double YMean = Sy / N;
  if(Qx==0.0)
    return false;
  a = Qxy / Qx;
  b = YMean - a * XMean;
	return true;
}

double FInverse(double p)
{
//'FInverse - ,     
//'        0,1 ( -  4- ), 0<p<1
	double t,rez;
  if((p <= 0) ||( p >= 1))
    return 0.0;
	if (p > 0.5) 
		t = sqrt(log(1 / ((1 - p) * (1 - p))));
	else
		t = sqrt(log(1 / (p * p)));
	rez = t - (0.010328 * t * t + 0.802853 * t + 2.515517) / (1 + 1.432788 * t + 0.189269 * t * t + 0.001308 * t * t * t);
  if (p < 0.5)
    rez = -rez;
	return rez;
}

double FindMaxTri( const std::vector<std::vector<double> >& a, int &i0, int&j0)
{
  size_t N = a.size();
  if(N<2)
    return 0;
  i0 = 0;
  j0 = 1;
  double res = std::fabs(a[0][1]);
  for(size_t i = 0; i<N; i++) {
    for(size_t j=i + 1; j<N; j++) {
      if(std::fabs(a[i][j]) > res) {
        i0 = i;
        j0 = j;
        res = std::fabs(a[i][j]);
      }
    }
  }
  return res;
}

} //namespace pca
