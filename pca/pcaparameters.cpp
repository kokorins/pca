#include "pcaparameters.h"
#include <matrix/matrix.h>
#include <matrix/matrixop.h>
#include <cmath>
#include <vector>
#include <algorithm>
#include <functional>

namespace pca {
void PcaParameters::calc(const std::vector<double>& x, std::vector<double>&comp) const
{
  using namespace matrix;
  std::vector<double> x_loc(x.size());
  std::transform(x.begin(), x.end(), means.begin(), x_loc.begin(), std::minus<double>());
  if(is_std)
    for(size_t i=0; i<vars.size(); ++i)
      x_loc[i]/=std::sqrt(vars[i]/df);
  comp = MatrixOp::Mult(Matrix(lambda), x_loc);
  comp.resize(num_comp);
}
}//namespace pca
