#ifndef CALCULATIONFUNCTIONS_H_
#define CALCULATIONFUNCTIONS_H_
#include <vector>

namespace pca {
enum PCAErrorEnum
{
  _PCAErrNoErrors	= 0,
  _PCAErrUnknown	= -1,
  _PCAErrBadNumber	= 1,
  _PCAErrLowMemory	= 2,
  _PCAErrWrongDataType	= 3,
  _PCAErrWrongCaseNumber	= 4,
  _PCAErrWrongDimmention	= 5,
  _PCAErrManyCases	= 6,
  _PCAErrWrongComponentNumber	= 7,
  _PCAErrCalculation	= 8,
  _PCAErrStandardization	= 9,
  _PCAErrBusy	= 10,
  _PCAErrConnectionPoint	= 11
} 	/*PCAErrorEnum*/;

//'find maxAbs(A(i,j)),i<j
double FindMaxTri(const std::vector<std::vector<double> >& A, int &i0, int&j0);
double Signum(double x);
int Jakobi(const std::vector<std::vector<double> >& Abeg, 
           std::vector<double>& Lambda, 
           std::vector<std::vector<double> >& Xlambda);
//'Reorder values  in array Lambda(n),strings in array Xlambda(n,n)
//'in order of reduction Lambda(),n-dimension of arrays
void Reorder(std::vector<double>& lambda,
            std::vector<std::vector<double> >& Xlambda);
double Ndist(double n);
double Gamm(int n);
double FisherInverse(double p ,int n1 ,int n2);
double Fdist(double F,int n1,int n2);
bool LinRegr(const std::vector<double> X, const std::vector<double>& Y,
             double &a,double &b);
static double pi = 3.14159265358979;
} // namespace pca
#endif //CALCULATIONFUNCTIONS_H_
