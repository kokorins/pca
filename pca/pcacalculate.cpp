#include "pcacalculate.h"
#include "pcaparameters.h"
#include "calculationfunctions.h"
#include <cmath>
#include <vector>
#include <algorithm>
#include <numeric>
#include <functional>
#include <boost/bind.hpp>
#include <boost/shared_array.hpp>

namespace pca {

class DataModel {
public:
  DataModel();
  bool isInit()const;
public:
  void scoresLoadings(PcaParameters*const params=0);
  bool calcCovMat();
public:
  long num_vars;
  long num_cases;
  boost::shared_array<double> data; // data, sizeof num_cases * num_vars
  boost::shared_array<double> mean;	// mean values for variables
  boost::shared_array<double> ss; // sqrt of variances of variables.
  std::vector<std::vector<double> > cov; // covariation matrix
  bool	is_std;
  double	num_eig;
  double  sig_level;
  //principal components output data
  int num_comp;
  //eigen vectors
  std::vector<std::vector<double> > factors;
  //factor coordinates of cases
  std::vector<std::vector<double> > scores;
  //lambda - eigen values
  std::vector<double> lambdas;
};


DataModel::DataModel(): num_vars(0), num_cases(0), num_comp(0), is_std(true),
                        num_eig (0.5), sig_level (0.05) {}

bool PcaCalculate::calculate(PcaParameters * const params)
{
  if(!_data->isInit())
		return false;
  if(!_data->calcCovMat())
		return false;
  _data->scoresLoadings(params);
  return true;
}

double PcaCalculate::calcExplained(size_t num_comp) const
{
  double explainedSum = std::accumulate(_data->lambdas.begin(), _data->lambdas.begin()+num_comp, 0);
  double totalSum = std::accumulate(_data->lambdas.begin()+num_comp,_data->lambdas.end(), explainedSum);
  return explainedSum/totalSum;
}

long PcaCalculate::numVars() const
{
  return _data->num_vars;
}

const std::vector<std::vector<double> > &PcaCalculate::factors() const
{
  return _data->factors;
}

const std::vector<std::vector<double> > &PcaCalculate::scores() const
{
  return _data->scores;
}

void DataModel::scoresLoadings(PcaParameters*const params)
{
  double nc1=num_cases-1;
  std::vector<double> lambda(num_vars);
  std::vector<std::vector<double> > Xlambda(num_vars, std::vector<double>(num_vars));
  std::vector<std::vector<double> > corr_mat(num_vars, std::vector<double>(num_vars));
  scores.resize(num_vars);
  for(size_t i=0; i<scores.size(); ++i)
    scores[i].resize(num_cases);
  factors.resize(num_vars);
  for(size_t i=0; i<factors.size(); ++i)
    factors[i].resize(num_vars);
  lambdas.resize(num_vars);

// calculation
  if(is_std) {
    for(int i=0; i<num_vars; i++) {
      for(int j=0; j<num_vars; j++)
        corr_mat[i][j] = cov[i][j]/sqrt(cov[i][i]*cov[j][j]);
		}
	}
	else {			// CorrM - covariation matrix
    if(nc1 <= 0.0)
      nc1 = 1.0;
    for(int i=0; i<num_vars; i++) {
      for(int j=0; j<num_vars; j++)
        corr_mat[i][j] = cov[i][j]/nc1;
		}
	}
  Jakobi(corr_mat, lambda, Xlambda);
  Reorder(lambda, Xlambda);
  lambdas = lambda;
  num_comp = std::count_if(lambda.begin(), lambda.end(),
                        boost::bind(std::greater_equal<double>(), _1, num_eig));
  factors = Xlambda;
  if(params) {
    params->num_comp = num_comp;
    params->df = (size_t)nc1;
    params->means.resize(num_vars);
    std::copy(mean.get(), mean.get()+num_vars, params->means.begin());
    params->vars.resize(num_vars);
    std::copy(ss.get(), ss.get()+num_vars, params->vars.begin());
    params->is_std = is_std;
    params->lambda.resize(num_comp);
    for(size_t i=0; i<params->lambda.size(); ++i) {
      params->lambda[i].resize(num_vars);
      std::copy(Xlambda[i].begin(), Xlambda[i].end(), params->lambda[i].begin());
    }
  }
  for(int j=0; j<num_vars; ++j) {
    for(int i=0; i<num_cases; i++) {
			int nn=num_vars*i;
      for(int k=0; k<num_vars; k++) {
				double xx=data[nn+k]-mean[k];
        if(is_std)
          xx/=sqrt(ss[k]/nc1);
        scores[j][i] += Xlambda[j][k]*xx;
			}
		}
  }
}

bool DataModel::calcCovMat()
{
  if(num_cases < 2 )
    return false;
  for(long i=0; i<num_cases; ++i) {
    long nn=i*num_vars;
    for(long j=0; j<num_vars; ++j) {
      double prom = data[nn+j]-mean[j];
      mean[j] += prom / (i+1);
      for(long k=0; k<=j; ++k)
        cov[k][j] += prom*(data[nn+k]-mean[k]);
    }
  }
//calculate SS
  for(long j=0; j<num_vars; j++) {
    ss[j] = cov[j][j];
    for(long k=j+1; k<num_vars; k++)
      cov[k][j]=cov[j][k];
  }
  return true;
}

bool DataModel::isInit() const
{
  return !(num_vars <= 0 || num_cases <= 0 || !data);
}

bool PcaCalculate::setData(long NCase, long NVariables)
{
  _data->num_vars = NVariables;
  _data->num_cases = NCase;
  if( _data->num_vars <= 0 || _data->num_cases <= 0 )
		return false;
  int N = _data->num_vars*_data->num_cases;
  _data->data.reset(new double[N]);
  std::fill(_data->data.get(), _data->data.get()+N, 0);
  _data->mean.reset(new double[_data->num_vars]);
  std::fill(_data->mean.get(), _data->mean.get()+_data->num_vars, 0);
  _data->ss.reset(new double[_data->num_vars]);
  std::fill(_data->ss.get(), _data->ss.get()+_data->num_vars, 0);
  _data->cov.resize(_data->num_vars, std::vector<double>(_data->num_vars));
	return true;
}

bool PcaCalculate::setData(long NCase, long NVariables,
                           const std::vector<std::vector<double> >& data)
{
  if(!setData(NCase, NVariables))
    return false;
  for(int i=0; i< NCase; ++i)
    for (int j=0; j<NVariables; ++j)
      _data->data[i*NVariables+j] = data[j][i];
  return true;
}

bool PcaCalculate::setData(const std::vector<std::vector<double> > &data)
{
  if(data.empty())
    return false;
  if(data.front().empty())
    return false;
  return setData(data.front().size(), data.size(), data);
}


long PcaCalculate::numCases() const
{
  return _data->num_cases;
}

bool PcaCalculate::setCase(long idx, const std::vector<double>& vars)
{
  if(_data->isInit())
    return false;
  if(idx >= _data->num_cases)
    return false;
  int n=idx*_data->num_vars;
  for(int i=0;i<_data->num_vars;i++,n++)
    _data->data[n] = vars[i];
  return true;
}

bool PcaCalculate::setCase(const std::vector<std::vector<double> >& Variables)
{
  if(!_data->isInit())
    return false;
  for(long j=0;j<_data->num_cases;j++) {
    long n=j*_data->num_vars;
    for(long i=0; i<_data->num_vars; i++,n++)
      _data->data[n] = Variables[i][j];
	}
	return true;
}

bool PcaCalculate::isStandardize() const
{
  return _data->is_std;
}

void PcaCalculate::setStandardize(bool val)
{
  _data->is_std=val;
}

double PcaCalculate::limEigValue() const
{
  return _data->num_eig;
}

void PcaCalculate::setLimEigValue(double val)
{
  _data->num_eig=val;
}

// standardization data 
bool PcaCalculate::standardize()
{
  if(!_data->isInit())
    return false;
  int i,j,n;
  int nv=_data->num_vars;
  int nc=_data->num_cases;
	if( nc <= 1 ) 
		return false;
	for(i=0;i<nv;i++) {
    _data->mean[i] = 0.0;
		for(j=0;j<nc;j++) {
			n=nv*j+i;
      _data->mean[i]+=_data->data[n];
		}
    _data->mean[i]/=nc;
		for(j = 0;j<nc;j++) {
			n=nv*j+i;
      _data->data[n]-=_data->mean[i];
		}
		
    _data->ss[i] = 0.0;
    for(j=0; j<nc; j++) {
			n=nv*j+i;
      double elem = _data->data[n];
      _data->ss[i]+=(elem*elem);
		}
		
    _data->ss[i]/=nc-1;
    double elem = 1.0/std::sqrt(_data->ss[i]);
    for(j=0; j<nc; j++){
			n=nv*j+i;
      _data->data[n]*=elem;
		}
	}//Next i
	return true;
}

// Get standardization data 2-dimension array
bool PcaCalculate::getStdCases(std::vector<std::vector<double> >& cases)const
{
  if(!_data->isInit())
    return false;
  cases.resize(_data->num_cases);
  for(size_t i=0; i<cases.size(); ++i)
    cases.resize(_data->num_vars);
  for(long i=0; i<_data->num_cases; ++i) {
    int nn= i*_data->num_vars;
    for(long j=0; j<_data->num_vars; ++j)
      cases[i][j] = _data->data[nn+j];
	}
	return true;
}

// Get standardization means
bool PcaCalculate::getStdMeans(std::vector<double>& Means, std::vector<double>& SS) const
{
  if(!_data->isInit())
		return false;
  SS.resize(_data->num_vars);
  std::copy(_data->ss.get(), _data->ss.get()+_data->num_vars, SS.begin());
  Means.resize(_data->num_vars);
  std::copy(_data->mean.get(), _data->mean.get()+_data->num_vars, Means.begin());
	return true;
}

double PcaCalculate::sigLevel() const
{
  return _data->sig_level;
}

void PcaCalculate::setSigLevel(double val)
{
  _data->sig_level=val;
}

bool PcaCalculate::calcHottellingEllipse(int idx_x, int idx_y, double &XAxes, double &YAxes)
{
  if(_data->scores.size()<2)
		return false;
  int n=_data->num_cases;
  double ma = std::accumulate(_data->scores[idx_x].begin(), _data->scores[idx_x].end(), 0.0);
  double mb = 0;
  if(idx_x==idx_y)
    mb = ma;
  else
    mb = std::accumulate(_data->scores[idx_y].begin(), _data->scores[idx_y].end(), 0.0);
	ma/=n;
  mb/=n;
  double da = std::inner_product(_data->scores[idx_x].begin(), _data->scores[idx_x].end(),
                                 _data->scores[idx_x].begin(), 0.0); // sum squares
  double db = 0;
  if(idx_x==idx_y)
    db = da;
  else
    db = std::inner_product(_data->scores[idx_y].begin(), _data->scores[idx_y].end(),
                            _data->scores[idx_y].begin(), 0.0);
  da = da/n - ma*ma;
  db = db/n - mb*mb;
  XAxes=std::sqrt(da*FisherInverse(_data->sig_level,2,n-2)*2*(n*n-1.0)/(n*(n-2.0)));
  YAxes=std::sqrt(db*FisherInverse(_data->sig_level,2,n-2)*2*(n*n-1.0)/(n*(n-2.0)));
	return true;
}


bool PcaCalculate::getScores(size_t idx, std::vector<double>& scores) const
{
  if(_data->scores.size()>=idx)
    return false;
  scores = _data->scores[idx];
  return true;
}

bool PcaCalculate::getLoadings(size_t idx, std::vector<double>& loadings) const
{
  if(_data->factors.size()>=idx)
    return false;
  loadings = _data->factors[idx];
  return true;
}

PcaCalculate::PcaCalculate() : _data(new DataModel()) {}

long PcaCalculate::numComps() const
{
  return _data->num_comp;
}

const std::vector<double> &PcaCalculate::lambda() const
{
  return _data->lambdas;
}
}//namespace pca
