#ifndef PCAPARAMETERS_H_
#define PCAPARAMETERS_H_
#include <vector>
#include <cstddef>

namespace pca {
struct PcaParameters {
  size_t num_comp;
  size_t df;
  std::vector<double> means;
  std::vector<double> vars;
  bool is_std;
  std::vector<std::vector<double> > lambda;
  void calc(const std::vector<double>& x, std::vector<double>&comp)const;
};
} //namespace pca
#endif //PCAPARAMETERS_H_
