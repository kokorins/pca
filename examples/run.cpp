#include <pca/pcacalculate.h>
#include <pca/pcaparameters.h>
#include <vector>
#include <cstddef>
#include <iostream>
#include <iterator>

void pca_test() {
  pca::PcaCalculate pc;
  pca::PcaParameters params;
  size_t num_vars = 2;
  size_t num_cases = 4;
  std::vector<std::vector<double> > data(num_vars, std::vector<double>(num_cases));
  for(int i=0; i<4; ++i) {
    if(i%2)
      data[0][i]=1;
    if((i>>1)%2)
      data[1][i]=1;
    std::cout<<data[0][i]<<data[1][i]<<std::endl;
  }
  pc.setData(data);
  pc.calculate(&params);
  std::cout<<"Eigen values: ";
  std::copy(pc.lambda().begin(), pc.lambda().end(), std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"Explained: "<<pc.calcExplained(1)<<std::endl;
  for(size_t i=0; i<pc.factors().size(); ++i) {
    std::copy(pc.factors()[i].begin(), pc.factors()[i].end(), std::ostream_iterator<double>(std::cout," "));
    std::cout<<std::endl;
  }
}

void pca_test2()
{
  pca::PcaCalculate pc;
  size_t num_vars = 2;
  size_t num_cases = 4;
  std::vector<std::vector<double> > data(num_vars, std::vector<double>(num_cases));
  for(size_t i=0; i<num_cases; ++i) {
    for(size_t j=0; j<num_vars; ++j) {
      data[j][i] = i;
    }
  }
  pc.setData(data);
  pc.calculate();
  for(size_t i=0; i<pc.scores().size(); ++i) {
    std::copy(pc.scores()[i].begin(), pc.scores()[i].end(), std::ostream_iterator<double>(std::cout," "));
    std::cout<<std::endl;
  }
}

int main() {
  pca_test2();
  return 0;
}
